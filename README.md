# BudgetGroup

## Contexte

Nous avons commencé par nous poser les questions pour évaluer le contexte du projet.
Afin de cibler au mieux les besoins utilisateurs, nous avons établi les étapes suivantes :

    - Fonctionnalités obligatoires ==>  
      1. Les employés :  
        - Sélectionner par catégorie
        - Sélectionner un article et sa quantité
        - Retirer un article
        - Visualiser le montant total
        - Envoyer la commande à celui qui la valide
      2. Le Chef de service :
         - Valider ou refuser la commande
      3. Le service achat : 
         - Paiement de la commande
      4. Comptabilité et DG : 
         - Visualiser l'historique des recettes
         - Visualiser l'historique des dépenses
         - Visualiser les dépenses par catégorie 
         - Visualiser les dépenses par service

- Pour qui ?
  - Les employés, usage interne.

## Conception

    1. Diagramme de UseCase :
        Nous avons comméncé par réaliser une digramme d'étude de cas afin de cibler précisément les fonctionnalités requises à l'application.

    2. Diagramme de classe (entités modèles) :
        Pour suivre, nous avons élaboré les entités qui feront écho à nos tables en BDD.

    3. Diagramme de classe détaillé :
        Concernant le "diagramme de classe détaillé", il est composé des classes faisant reference aux entités du diagramme de la "classe modèle", suivi des controlleurs et répositories. 

    4. Diagramme d'activité :
        Pour continuer, nous avons approfondi notre conception avec un diagramme d'activité représentant la réalisation d'une commande par un employé de la société. 

    5. Diagramme de séquence :
        Et pour terminer, nous avons réalisé un diagramme de séquence qui fait écho au diagramme d'activité. 
        Il contient le procédé de la vie des méthodes lors d'une commande.
